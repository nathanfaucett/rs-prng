#![feature(test)]


extern crate test;
extern crate rand;
extern crate num_cpus;

extern crate prng;
extern crate rng_trait;


use test::Bencher;


static SIZE: usize = 1024;


#[bench]
fn test_prng(b: &mut Bencher) {
    use rng_trait::Rng;
    use prng::Prng;
    let mut random = Prng::new();

    b.iter(|| {
        for _ in 0..SIZE {
            test::black_box(random.next_f64());
        }
    });
}
#[bench]
fn test_rand(b: &mut Bencher) {
    use rand::Rng;
    let mut random = rand::weak_rng();

    b.iter(|| {
        for _ in 0..SIZE {
            test::black_box(random.next_f64());
        }
    });
}


#[bench]
fn test_thread_prng(b: &mut Bencher) {
    use rng_trait::Rng;
    use prng::AtomicPrng;
    let mut random = AtomicPrng::new();

    b.iter(move || {
        for _ in 0..SIZE {
            test::black_box(random.next_f64());
        }
    });
}
#[bench]
fn test_thread_rand(b: &mut Bencher) {
    use rand::Rng;
    let mut random = rand::thread_rng();

    b.iter(move || {
        for _ in 0..SIZE {
            test::black_box(random.next_f64());
        }
    });
}
