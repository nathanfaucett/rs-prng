rs-prng
=====
pseudo random number generator

```rust
extern crate prng;
extern crate rng_trait;


use rng_trait::Rng;
use prng::Prng;


fn main() {
    // seeds from an address of a local variable
    let mut random = Prng::new();

    // some random unsigned integer
    // random.set_seed(24564764657);

    println!("x = {:?}", random.next()); // usize number between 0 and usize MAX
    println!("y = {:?}", random.next_f32()); // f32 between 0 and 1
    println!("z = {:?}", random.next_f64()); // f64 between 0 and 1
}
```
